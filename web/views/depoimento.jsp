<%-- 
    Document   : depoimento
    Created on : May 4, 2016, 3:33:30 PM
    Author     : Ricardo Maes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="${resources}/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="${resources}/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="${resources}/css/estilo.css"/>
        <title>Ricardo Maes Zaguini Nunes</title>
        <style>
            body {
                background-image: url(${resources}/img/bricks.jpg);
                background-repeat: repeat;
            }
        </style>
    </head>
    <body>
        <jsp:include page="header.jsp">
            <jsp:param name="selected" value="btn-contato"/>
        </jsp:include>
        <div class="container content">
            <ol class="breadcrumb">
                <li><a href="#">Meu Site</a></li>
                <li class="active">Depoimentos</li>
            </ol>
            <c:if test="${error != null}">
            <div class="alert alert-danger fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <span><c:out value="${error.toString()}"/></span>
            </div>
            </c:if>
            <div class="page-header">
                <h1>Depoimentos</h1>
            </div>
            <div id="depoimentos">
                <c:if test="${depoimentos.isEmpty()}">
                    <h2 class="text-muted">Nenhum depoimento ainda...</h2><br><br>
                </c:if>
                <c:forEach items="${depoimentos}" var="dep">
                    <blockquote>
                        <div class="row">
                            <div class="col-md-11 col-xs-10">
                                <h2 class="text-capitalize"><c:out value="${dep.name}:"/></h2>
                            </div>
                            <div class="col-md-1 col-xs-2">
                                <img class="gravatar img-circle img-responsive" title="<c:out value="${dep.email}"/>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-muted"><c:out value="${dep.comment}"/></p>
                                <c:if test="${user.isAdmin}">
                                <a class="pull-right" href="${context}/depoimento/delete?id=${dep.id}" title="Remover este depoimento"><span class="glyphicon glyphicon-remove text-danger"></span></a>
                                </c:if>
                            </div>
                        </div>
                    </blockquote>
                </c:forEach>
            </div>
            <h2>Deixe o seu:</h2>
            <form id="comment_form" method="post" action="${context}/depoimento/submit">
                <div class="input-group">
                    <label for="input-name" class="input-group-addon">Nome:</label>
                    <input type="text" id="input-name" name="name" class="form-control" placeholder="Foo Bar da Silva">
                    <label id="name_feedback" for="input-name" class="input-group-addon" data-toggle="tooltip" data-placement="left" title="O nome não pode ser vazio">
                        <span class="glyphicon glyphicon-alert icon-color-warning"></span>
                    </label>
                </div>
                <div class="input-group">
                    <label for="input-email" class="input-group-addon">E-mail:</label>
                    <input type="email" id="input-email" name="email" class="form-control" placeholder="foobar@exemplo.com">
                    <label for="input-email" id="email_feedback" class="input-group-addon" data-toggle="tooltip" data-placement="left" title="O endereço de E-mail é inválido">
                        <span class="glyphicon glyphicon-alert icon-color-warning"></span>
                    </label>
                </div>
                <hr>
                <div class="input-group">
                    <textarea id="input-comment" class="form-control resize-vert" rows="12" name="comment" placeholder="Escreva aqui"></textarea>
                    <label for="input-comment" id="comment_feedback" class="input-group-addon" data-toggle="tooltip" data-placement="left" title="O comentário deve conter ao menos 10 caracteres">
                        <span class="glyphicon glyphicon-alert icon-color-warning"></span>
                    </label>
                </div>
                <hr>
                <button type="submit" class="btn btn-primary pull-right" onclick="confirm_comment()" disabled>Enviar</button>
            </form>
        </div>
        <br>
        <footer class="page-footer">
            <div class="container">
                Ricardo Maes Zaguini Nunes © 2016
            </div>
        </footer>
        <script src="${resources}/js/jquery.min.js"></script>
        <script src="${resources}/js/bootstrap.min.js"></script>
        <script src="${resources}/js/md5.min.js"></script>
        <script src="${resources}/js/script.js"></script>
        <script>
            $(document).ready(function() {
                $('#btn-contato').addClass('active');
                $('.gravatar').each(function() {
                    var email = $(this).attr('title');
                    if(email != '') {
                        $(this).attr('src', 'http://www.gravatar.com/avatar/' + md5(email));
                    }
                });
            });
        </script>
    </body>
</html>
