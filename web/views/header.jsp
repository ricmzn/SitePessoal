<%-- 
    Document   : header
    Created on : May 4, 2016, 9:33:07 PM
    Author     : ricardo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${user == null}">
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="${context}/usuario/login">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                    <h4 class="modal-title">Login</h4>
                </div>
                <div class="modal-body">
                    <div class="input-group">
                        <label for="input-login-username" class="input-group-addon">Usuário:</label>
                        <input id="input-login-username" type="text" name="username" class="form-control">
                    </div>
                    <div class="input-group">
                        <label for="input-login-password" class="input-group-addon">Senha:</label>
                        <input id="input-login-password" type="password" name="password" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>
</c:if>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="nav">
            <ul class="nav navbar-nav navbar-left">
                <li id="btn-home"><a href="#">Home</a></li>
                <li id="btn-projetos"><a href="#">Portfólio</a></li>
                <li id="btn-contato"><a href="#">Depoimentos</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <c:choose>
                <c:when test="${user != null}">
                <li class="dropdown">
                    <button class="dropdown-toggle btn navbar-btn btn-success dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-user"></span>
                        <span>&nbsp;<c:out value="${user.displayName}"/>&nbsp;</span>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="${context}/usuario/logout" onclick="logout()"><span class="glyphicon glyphicon-log-out text-danger"></span>&nbsp;Sair</a>
                        </li>
                    </ul>
                </li>
                </c:when>
                <c:otherwise>
                <li>
                    <a href="#" data-toggle="modal" data-target="#login-modal">Entrar</a>
                </li>
                </c:otherwise>
                </c:choose>
            </ul>
        </div>
    </div>
</nav>
