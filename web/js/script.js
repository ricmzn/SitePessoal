$(document).ready(function(){
    $("[data-toggle='tooltip']").tooltip(); 
});

var name_input_ok = false;
var email_input_ok = false;
var comment_input_ok = false;

function set_tooltip(element, text) {
    element.attr("data-original-title", text);
    element.tooltip("hide");
}

function unset_tooltip(element) {
    set_tooltip(element, "");
}

function set_icon(element, is_ok) {
    var child_span = element.find("span");
    if(is_ok) {
        child_span.removeClass("glyphicon-alert icon-color-warning");
        child_span.addClass("glyphicon-ok icon-color-ok");
    }
    else {
        child_span.removeClass("glyphicon-ok icon-color-ok");
        child_span.addClass("glyphicon-alert icon-color-warning");
    }
}

function enable_submit_if_valid() {
    var button = $("#comment_form button[type='submit']");
    if(name_input_ok && email_input_ok && comment_input_ok) {
        button.prop("disabled", false);
    }
    else {
        button.prop("disabled", true);
    }
}

// Nome não pode ser vazio
function validate_name(text) {
    return text.length > 0;
}

// Email deve conter arroba e domínio
function validate_email(text) {
    return text.search(/^.+@.+\..+$/) >= 0;
}

// Comentário de conter ao menos 10 caracteres
function validate_comment(text) {
    return text.length >= 10;
}

// Validação do nome
$("#comment_form input[name='name']").on("input", function(event) {
    var feedback = $("#name_feedback");
    if(validate_name($(this).val())) {
        name_input_ok = true;
        unset_tooltip(feedback);
        set_icon(feedback, true);
    }
    else {
        name_input_ok = false;
        set_tooltip(feedback, "O nome não pode ser vazio");
        set_icon(feedback, false);
    }
    enable_submit_if_valid();
});

// Validação do E-mail
$("#comment_form input[name='email']").on("input", function(event) {
    var feedback = $("#email_feedback");
    if(validate_email($(this).val())) {
        email_input_ok = true;
        unset_tooltip(feedback);
        set_icon(feedback, true);
    }
    else {
        email_input_ok = false;
        set_tooltip(feedback, "O endereço de E-mail é inválido");
        set_icon(feedback, false);
    }
    enable_submit_if_valid();
});

// Validação do comentário
$("#comment_form textarea[name='comment']").on("input", function(event) {
    var feedback = $("#comment_feedback");
    if(validate_comment($(this).val())) {
        comment_input_ok = true;
        unset_tooltip(feedback);
        set_icon(feedback, true);
    }
    else {
        comment_input_ok = false;
        set_tooltip(feedback, "O comentário deve conter ao menos 10 caracteres");
        set_icon(feedback, false);
    }
    enable_submit_if_valid();
});
