package domain;

public class Usuario
{
    String username;
    String password;

    public Usuario(String username, String password)
    {
        this.username = username;
        this.password = password;
    }

    public String getUsername()
    {
        return username;
    }

    public String getDisplayName()
    {
        return username.substring(0, 1).toUpperCase() + username.substring(1);
    }

    public String getPassword()
    {
        return password;
    }

    public boolean getIsAdmin()
    {
        return "admin".equals(username);
    }
}
