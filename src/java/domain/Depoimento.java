package domain;

public class Depoimento
{
    private static int LAST_ID = 0;

    String name;
    String email;
    String comment;
    int id;

    public Depoimento(String name, String email, String comment)
    {
        this.name = name;
        this.email = email;
        this.comment = comment;
        this.id = LAST_ID++;
    }

    public String getName()
    {
        return name;
    }

    public String getEmail()
    {
        return email;
    }

    public String getComment()
    {
        return comment;
    }

    public Integer getId()
    {
        return id;
    }
}
