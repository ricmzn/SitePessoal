package control;

import domain.Depoimento;
import domain.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.*;
import javax.servlet.*;

public class DepoimentoController
{
    HttpServletRequest request;
    HttpServletResponse response;
    ServletContext context;

    public DepoimentoController(HttpServletRequest request, HttpServletResponse response, ServletContext context)
    {
        this.request = request;
        this.response = response;
        this.context = context;
    }

    private List<Depoimento> getDepoimentos()
    {
        List<Depoimento> depoimentos = (List<Depoimento>) request.getSession().getAttribute("depoimentos");
        if(depoimentos == null) {
            depoimentos = new ArrayList<>();
        }
        return depoimentos;
    }

    public void list(String contextPath) throws Exception
    {
        request.setAttribute("depoimentos", getDepoimentos());
        request.setAttribute("error", request.getSession().getAttribute("error"));
        request.getSession().removeAttribute("error");
        RequestDispatcher dsp = request.getRequestDispatcher("/views/depoimento.jsp");
        dsp.forward(request, response);
    }

    public void submit(String contextPath) throws Exception
    {
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String comment = request.getParameter("comment");
        if(name != null && email != null && comment != null && !name.isEmpty() && email.matches(".+@.+\\..+")) {
            List<Depoimento> depoimentos = getDepoimentos();
            depoimentos.add(new Depoimento(name, email, comment));
            request.getSession().setAttribute("depoimentos", depoimentos);
        }
        else {
            request.getSession().setAttribute("error", "Os dados inseridos são inválidos, tente novamente");
        }
        response.sendRedirect(contextPath + "/depoimento/list");
    }

    public void delete(String contextPath) throws Exception
    {
        Usuario user = (Usuario) request.getSession().getAttribute("user");
        if(user != null && user.getIsAdmin()) {
            try {
                Integer id = Integer.parseInt(request.getParameter("id"));
                List<Depoimento> depoimentos = getDepoimentos();
                for(Depoimento dep : depoimentos) {
                    if(dep.getId().equals(id)) {
                        depoimentos.remove(dep);
                        break;
                    }
                }
                request.getSession().setAttribute("depoimentos", depoimentos);
            }
            catch(NumberFormatException ex) {
                request.getSession().setAttribute("error", "Depoimento inválido");
            }
        }
        else {
            request.getSession().setAttribute("error", "Você não tem permissão para fazer esta ação");
        }
        response.sendRedirect(contextPath + "/depoimento/list");
    }
}
