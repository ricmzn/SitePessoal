package control;

import java.io.IOException;
import java.lang.reflect.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;
import javax.servlet.*;

@WebServlet(name = "FrontController",
        urlPatterns = {"/app/*"},
        initParams = {
            @WebInitParam(name = "usuario", value = "control.UsuarioController"),
            @WebInitParam(name = "depoimento", value = "control.DepoimentoController")
        }
)

public class FrontController extends HttpServlet
{

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String contextPath = getServletContext().getContextPath() + "/app";
        request.setAttribute("resources", getServletContext().getContextPath());
        request.setAttribute("context", contextPath);

        String path = request.getRequestURI();
        int pos1 = path.lastIndexOf("/");
        int pos2 = path.lastIndexOf("/", pos1 - 1);
        String action = path.substring(pos1 + 1, path.length());
        String module = path.substring(pos2 + 1, pos1);

        String className = getInitParameter(module);
        try {
            Class<Object> controllerClass = (Class<Object>) Class.forName(className);
            Constructor<Object> constructor = controllerClass.getConstructor(HttpServletRequest.class, HttpServletResponse.class, ServletContext.class);
            Object controller = constructor.newInstance(request, response, getServletContext());
            Class[] parameters = {String.class};
            Method method = controller.getClass().getDeclaredMethod(action, parameters);
            method.invoke(controller, contextPath);
        }
        catch(InvocationTargetException error) {
            response.setHeader("Content-Type", "text/plain");
            error.getCause().printStackTrace(response.getWriter());
        }
        catch(Exception error) {
            response.setHeader("Content-Type", "text/plain");
            error.printStackTrace(response.getWriter());
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
