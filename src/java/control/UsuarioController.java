package control;

import domain.Usuario;
import javax.servlet.http.*;
import javax.servlet.*;

public class UsuarioController
{
    HttpServletRequest request;
    HttpServletResponse response;
    ServletContext context;

    public UsuarioController(HttpServletRequest request, HttpServletResponse response, ServletContext context)
    {
        this.request = request;
        this.response = response;
        this.context = context;
    }

    public void login(String contextPath) throws Exception
    {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if(username != null && password != null && username.equals("admin") && password.equals("123456")) {
            Usuario user = new Usuario(username, password);
            request.getSession().setAttribute("user", user);
        }
        else {
            request.getSession().setAttribute("error", "Login inválido!");
        }
        response.sendRedirect(contextPath + "/depoimento/list");
    }

    public void logout(String contextPath) throws Exception
    {
        request.getSession().setAttribute("user", null);
        response.sendRedirect(contextPath + "/depoimento/list");
    }
}
