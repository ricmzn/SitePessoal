package filters;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebFilter;

@WebFilter(filterName = "ViewFilter", urlPatterns = {"/views/*"}, dispatcherTypes = {DispatcherType.REQUEST})
public class BlockFilter implements Filter
{
    FilterConfig config;

    @Override
    public void init(FilterConfig filterConfig)
    {
        config = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException
    {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        if(httpResponse != null) {
            String message = "";
            if(httpRequest != null) {
                message = httpRequest.getRequestURI();
            }
            httpResponse.sendError(404, message);
        }
    }

    @Override
    public void destroy()
    {
    }
}
